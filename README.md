# rust-temp-conversion

rust-temp-conversion is a mini project of mine that converts temperatures from one unit to another. Units included are calcius and fahrenheit.

The main purpose of this project was to learn Rust. This is the "first" Rust program I've ever created, aside from the usual "Hello World".

## Usage

Clone this repository to your local, and run `cargo run`

It will prompt you for a temperature, enter an input and it will display the conversion for you e.g.

```bash
Please enter a temperature you wish to convert, e.g. 55.1C: 13f
-10.56C
```

## Error handling

I paid special attention to using proper types. User input is converted to a `Temperature`, which has different methods available for validating input strings, displaying in a format string, etcetera. This allows for comprehensive error handling and allows for maximum composability.
