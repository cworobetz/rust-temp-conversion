mod conversion;

use conversion::Temperature;
use std::io::{self, Write};

fn main() {
    let temp: conversion::Temperature = get_input();
    let converted = temp.convert();
    println!("{}", converted);
}

// Loops until it receives non-empty input
fn get_input() -> Temperature {
    loop {
        let mut input: String = String::new();

        while input.trim().is_empty() {
            input = String::new();
            print!("Please enter a temperature you wish to convert, e.g. 55.1C: ");
            io::stdout().flush().expect("Failed to flush stdout"); // We need to flush stdout, since it wrote to the buffer but wasn't flushed by a newline
            io::stdin()
                .read_line(&mut input)
                .ok()
                .expect("Couldn't read line");
        }

        let input = input.trim().to_uppercase();

        match Temperature::validate(&input) {
            Ok(temperature) => return temperature,
            Err(e) => {
                println!("Error parsing temperature: {}", e)
            }
        };
    }
}
