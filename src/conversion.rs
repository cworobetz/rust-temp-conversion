use std::fmt;

#[derive(Copy, Clone)]
pub(crate) struct Temperature {
    pub(crate) value: f64,
    pub(crate) unit: Unit,
}
#[derive(Copy, Clone)]
pub(crate) struct Conversion {
    pub(crate) fahrenheit: Temperature,
    pub(crate) celcius: Temperature,
    pub(crate) kelvin: Temperature,
}

impl Temperature {
    pub fn convert(&self) -> Conversion {
        match self.unit {
            Unit::C => {
                let f = self.value * (9.0 / 5.0) + 32.0;
                let k = self.value + 273.15;
                let fa = Temperature {
                    value: f,
                    unit: Unit::F,
                };
                let ke = Temperature {
                    value: k,
                    unit: Unit::K,
                };
                Conversion {
                    kelvin: ke,
                    fahrenheit: fa,
                    celcius: *self,
                }
            }
            Unit::F => {
                let c = (self.value - 32.0) * (5.0 / 9.0);
                let k = (self.value - 32.0) * (5.0 / 9.0) - 273.15;
                let ce = Temperature {
                    value: c,
                    unit: Unit::C,
                };
                let ke = Temperature {
                    value: k,
                    unit: Unit::K,
                };
                Conversion {
                    celcius: ce,
                    fahrenheit: *self,
                    kelvin: ke,
                }
            }
            Unit::K => {
                let c = self.value + 273.15;
                let f = self.value * (9.0 / 5.0) + 32.0 + 273.15;
                let ce = Temperature {
                    value: c,
                    unit: Unit::C,
                };
                let fa = Temperature {
                    value: f,
                    unit: Unit::F,
                };
                Conversion {
                    celcius: ce,
                    fahrenheit: fa,
                    kelvin: *self,
                }
            }
        }
    }

    pub fn validate(input: &str) -> Result<Temperature, TemperatureError> {
        // Validate length
        let len = input.len();
        if len < 2 {
            return Err(TemperatureError::TooShort);
        }

        // Validate includes unit
        let unit = match input.chars().last().unwrap() {
            'C' => Unit::C,
            'F' => Unit::F,
            _ => {
                return Err(TemperatureError::NotAUnit);
            }
        };

        // Validate value is a float
        let val = input[..len - 1].to_string().parse::<f64>();
        match val {
            Ok(_) => {}
            Err(_) => return Err(TemperatureError::NotAValue),
        }
        let val = val.unwrap();

        // Validate val is a valid temperature in a given unit
        match unit {
            Unit::C => {
                if val < -273.15 {
                    return Err(TemperatureError::NotARealTemperature);
                }
            }
            Unit::F => {
                if val < -459.66 {
                    return Err(TemperatureError::NotARealTemperature);
                }
            }
            Unit::K => {
                if val < 0.0 {
                    return Err(TemperatureError::NotARealTemperature);
                }
            }
        }

        Ok(Temperature {
            value: val,
            unit: unit,
        })
    }
}

impl fmt::Display for Conversion {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.2}, {:.2}, {:.2}", self.celcius, self.fahrenheit, self.kelvin)
    }
}

impl fmt::Display for Temperature {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.2}{}", self.value, self.unit)
    }
}
pub(crate) enum TemperatureError {
    TooShort,
    NotAUnit,
    NotAValue,
    NotARealTemperature,
}

impl fmt::Display for TemperatureError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            &TemperatureError::TooShort => write!(f, "Too short to be any temperature"),
            &TemperatureError::NotAUnit => write!(f, "Not a real unit ('F' or 'C' or 'K')"),
            &TemperatureError::NotAValue => write!(f, "Not a valid value"),
            &TemperatureError::NotARealTemperature => write!(f, "Not a real temperature"),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub(crate) enum Unit {
    C,
    F,
    K,
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
